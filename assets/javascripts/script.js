const buttonTop = document.querySelector('.scroll-top')
window.addEventListener('scroll', e => {
	if (window.scrollY >= 100) buttonTop.classList.add('show')
	else buttonTop.classList.remove('show')
})
buttonTop.addEventListener('click', () => {
	window.scroll({ top: 0, left: 0 })
})

document.querySelector('form').addEventListener('submit', e => {
	e.preventDefault()
	const mesage = document.querySelector('.mesage')

	mesage.classList.add('show')

	setTimeout(() => mesage.classList.remove('show'), 3000)
})
